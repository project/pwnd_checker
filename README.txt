Pwned Checker
=============

Uses the the haveibeenpwned.com API to check if users accounts have been
compromised.

Checks the user e-mail when they login, and if the e-mail is part of a new
breach displays a message, suggesting to change their passwords and go to the
haveibeenpwned.com website for more information.

This module is not affiliated to haveibeenpwned.com.
